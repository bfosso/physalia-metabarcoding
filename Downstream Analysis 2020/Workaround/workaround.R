library(phyloseq)
library(vegan)
library(tidyverse)
library(picante)
library(dunn.test)

#INIZIAMO CON GLI EUBATTERI


# ASV table
ASVs <- read.csv("feature-table.tsv", sep="\t", header=TRUE, row.names=1, check.names = F, skip = 1)
# taxonomy
taxonomy<- read.csv("taxonomy.tsv", header = T, sep="\t", check.names = F)
taxtable <- separate(taxonomy, Taxon, sep=";", c("Kingdom","Phylum","Class","Order","Family","Genus","Species")) #convert the table into a tabular split version
taxtable <- as.matrix(data.frame(taxtable, row.names = "Feature.ID"))
tree <- read_tree("tree.nwk")
# sample data
metadata <- read.csv("sample-metadata.tsv", sep="\t", header=TRUE, row.names=1)
# phyloseq object
phy_obj <- phyloseq(otu_table(ASVs, taxa_are_rows = TRUE), tax_table(taxtable), sample_data(metadata), tree)
phy_obj
