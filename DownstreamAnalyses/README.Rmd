---
title: Downstream analyses in R
output:
  github_document:
    toc: true
  pdf_document: default
  html_document:
    df_print: paged
    theme: cosmo
    toc: true
    toc_depth: 3
    code_folding: hide
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, message = FALSE, warning = FALSE)
```

# Background
The Atacama soil microbiome data comes from an article in [__mSystems__](https://msystems.asm.org/), [*Significant Impacts of Increasing Aridity on the Arid Soil Microbiome*](https://msystems.asm.org/content/2/3/e00195-16)  

The authors studied key environmental and geochemical factors shaping the arid soil microbiome. The key findings are highlighted in the abstract below. For details about the study, you can have a look at the results, especially the figures. We will try to reproduce couple of them in this expercise. The figure 1 below shows the sampling locations.

__Abstract__:  

> Global deserts occupy one-third of the Earth’s surface and contribute significantly to organic carbon storage, a process at risk in dryland ecosystems that are highly vulnerable to climate-driven ecosystem degradation. The forces controlling desert ecosystem degradation rates are poorly understood, particularly with respect to the relevance of the arid-soil microbiome. Here we document __correlations between increasing aridity and soil bacterial and archaeal microbiome composition along arid to hyperarid transects__ traversing the Atacama Desert, Chile. A meta-analysis reveals that Atacama soil microbiomes exhibit a gradient in composition, are distinct from a broad cross-section of nondesert soils, and yet are similar to three deserts from different continents. __Community richness and diversity were significantly positively correlated with soil relative humidity (SoilRH). Phylogenetic composition was strongly correlated with SoilRH, temperature, and electrical conductivity.__ The strongest and most significant correlations between SoilRH and phylum relative abundance were observed for Acidobacteria, Proteobacteria, Planctomycetes, Verrucomicrobia, and Euryarchaeota (Spearman’s rank correlation [rs] = >0.81; false-discovery rate [q] = ≤0.005), characterized by 10- to 300-fold decreases in the relative abundance of each taxon. In addition, __network analysis revealed a deterioration in the density of significant associations between taxa along the arid to hyperarid gradient__, a pattern that may compromise the resilience of hyperarid communities because they lack properties associated with communities that are more integrated. In summary, results suggest that __arid-soil microbiome stability is sensitive to aridity__ as demonstrated by decreased community connectivity associated with the transition from the arid class to the hyperarid class and the __significant correlations observed between soilRH and both diversity and the relative abundances of key microbial phyla__ typically dominant in global soils.


![__Figure 1.__ Map of site locations for Baquedano (red) and Yungay (yellow) transects (Neilson *et al*, 2017) ](https://msystems.asm.org/content/msys/2/3/e00195-16/F1.large.jpg?width=800&height=600&carousel=1)




# Setup the workspace
__Before starting you should have downloaded your results to your own laptop using e.g. FileZilla.__

Change the workspace to the folder where you have your data with `setwd()`

```{r workspace, eval=FALSE}
setwd("/PATH/TO/DATA/")
```

Packages are collections of R functions. They are loaded with `library()` function.
```{r libraries}
library(phyloseq)
library(vegan)
library(tidyverse)
library(qiime2R)
library(picante)
library(gridExtra)
library(DESeq2)
```

If you're missing some of the packages, you can install most of them either from RStudio's dropdown menu `Tools -> Install Packages...`  
Or with `install.packages("PACKAGE_NAME")`.  

__tidyverse & vegan & picante & gridExtra:__
```{r tidyverse, eval=FALSE}
install.packages("tidyverse")
install.packages("vegan")
install.packages("picante")
install.packages("gridExtra")
```
Some of the packages need to be loaded from [Bioconductor](http://www.bioconductor.org/)  

__phyloseq & DESeq2:__
```{r phyloseq, eval=FALSE}
source('http://bioconductor.org/biocLite.R')
biocLite('phyloseq')
BiocManager::install("DESeq2", version = "3.8")
```
And some directly from GitHub

__qiime2R__:
```{r qiime2r, eval=FALSE}
install.packages("devtools")
library(devtools)
devtools::install_github("jbisanz/qiime2R")
```


After you have installed the missing packages, you still need to load each with `library`.  

# Load the data
Now everything should be ready and we can start working with the data.  
We will use a package called __*phyloseq*__ that is meant for working with microbiome data.  

First load the data into R and make a `phyloseq` object from it. Phyloseq object can contain an ASV table, a taxonomy table,a sample metadata table, a phylogenetic tree and all of the  ASV sequences.  

Our phyloseq object will contain the ASV table, the taxonomy table, the phylogenetic tree and the sample metadata table.  
```{r data}
metadata <- read_tsv("sample-metadata.tsv")
ASVs <- read_qza("table.qza")
taxonomy <- read_qza("taxonomy.qza")
tree <- read_qza("rooted-tree.qza")
phy_obj <- qza_to_phyloseq("table.qza", "rooted-tree.qza", "taxonomy.qza","sample-metadata.tsv")
phy_obj
```
We see that our phyloseq object contains ASV (OTU) table with 2847 taxa and 66 samples. Our sample data has 22 sample variables. The taxonomic annotations have 7 levels (Kingdom, Phylumn, Class, Order, Family, Genus, Species) . And finally we have a phylogenetic tree.

If you can't install `qiime2R`.
```{r, eval=FALSE}
# ASV table
ASVs <- read.table("ASV_table.txt", sep="\t", header=TRUE, row.names=1)
# taxonomy
taxonomy <- read.table("taxonomy.txt", sep="\t", header=TRUE, row.names=1)
taxonomy <- as.matrix(taxonomy)
# tree
tree <- read_tree("tree.nwk")
# sample data
metadata <- read.table("metadata.txt", sep="\t", header=TRUE, row.names=1)
# phyloseq object
phy_obj <- phyloseq(otu_table(ASVs, taxa_are_rows = TRUE), 
                    tax_table(taxonomy), sample_data(metadata), tree)
```

The taxonomic annotations look awful after QIIME2, so we better do something about it. Unfortunately we can't do much with the NA's, but at least we can get rid off the `k__`, `p__` etc...
```{r tax_table}
head(tax_table(phy_obj), 10)
tax_table(phy_obj) <- gsub(".__", "", tax_table(phy_obj))
head(tax_table(phy_obj), 10)
```

# Library sizes and normalisation

## Library size
To calculate library sizes we can use function `sample_sums`from phyloseq package. Since some of the libraries are pretty small, we need to decide which samples we'll keep.  
The line goes at 2000, probably smaller libraries should be removed from this study.
```{r norm}
sums <- sample_sums(phy_obj)
barplot(sums[order(sums, decreasing=TRUE)], las=3, cex.names = 0.5, ylab="Library size", col = "skyblue")
abline(h = 2000, lty=2)
```

## Proportions
We can again using a phyloseq function remove the samples with too small library size.  You can see that now we have 50 samples left.
You should in most cases (if not always) normalise your data one way or another. We will normalise the counts to proportions. Then all the counts add up to one.
````{r}
phy_obj <- prune_samples(sample_sums(phy_obj)>2000, phy_obj)
phy_obj
phy_norm <- transform_sample_counts(phy_obj, function(x) x/sum(x))
barplot(sample_sums(phy_norm), las=3, cex.names = 0.5, col="skyblue")
```

## Rarefying
Another way of normalising is to rarefy the counts. And for that there's also a rarefication function in phyloseq.  
We rarefy to the smallest library size after removing the smallest samples without replacement and trim all taxa that have zero count after rarefication.
```{r rarefy}
set.seed(771)
phy_raref <- rarefy_even_depth(phy_obj, sample.size = min(sample_sums(phy_obj)),
                               replace = FALSE, trimOTUs = TRUE, verbose = TRUE)
phy_raref
barplot(sample_sums(phy_raref), las=3, cex.names = 0.5, col = "skyblue")
```

```{r, include=FALSE}
rarecurve(t(otu_table(phy_obj)), step=100)
```

# Alpha diversity
Table of alpha diversity measures from the original publication can be downloaded from [here.](https://msystems.asm.org/content/msys/2/3/e00195-16/DC2/embed/inline-supplementary-material-2.pdf?download=true)  

In figure 2 the average soil relative humidity is plotted against two different alpha diverity measures. We will try to reproduce the figure.  

The Shannon index is quite robust to different sample sizes, so you can use either the normalised, rarefied or raw counts.  
For richness estimates we should use the rarefied data, since the seuqencing depth affects the observed richness.
```{r alpha, fig.width=5, fig.height=4}
# Diversity indices
shannon_div <- diversity(otu_table(phy_obj), MARGIN = 2, index = "shannon")
faiths_div <- pd(as.data.frame(t(otu_table(phy_raref))), phy_tree(phy_obj), include.root = FALSE)

# Average humidity
AvgHum <- sample_data(phy_obj)$AverageSoilRelativeHumidity

# Plot the richness against the average humidity
plot(faiths_div$SR~AvgHum, pch=21, cex=2, bg="skyblue", ylab="Faith's PD (Richness)",
     xlab="Average Soil Relative Humidity (%)")
cor.test(faiths_div$SR, AvgHum, method="spearman")

# And the Shannon diversity
plot(shannon_div~AvgHum, pch=21, cex=2, bg="skyblue", ylab="Shannon Diversity Index",  
     xlab="Average Soil Relative Humidity (%)")
cor.test(shannon_div, AvgHum, method="spearman")
```
We got pretty much the same results as in the original article even with just 10 % of the data.  


# Beta diversity
In figure 3 beta diversity is combined with soil properties. The beta diversity measure used is unweighted unifrac and the plot is PCoA.  
We'll use the `ordinate` to calculate the ordination and `plot_ordination`to plot it. In addtion we'll modiffy the plots using some ggplot2 functions.
```{r beta, fig.width=10, fig.height=8}
ord <- ordinate(phy_raref, method = "PCoA", distance = "unifrac", weighted=FALSE)

p1 <- plot_ordination(phy_norm, ord, color = "TransectName") + geom_point(size=5) +
  scale_color_manual(values=c("red", "yellow")) + theme_classic() + theme(legend.position = "bottom")

p2 <- plot_ordination(phy_norm, ord, color = "AverageSoilRelativeHumidity") + geom_point(size=5) +
  scale_color_continuous(low="yellow", high="brown", na.value = "white") + theme_classic() +
  theme(legend.position = "bottom")

p3 <- plot_ordination(phy_norm, ord, color = "TemperatureSoilHigh") + geom_point(size=5) +
  scale_color_continuous(low="yellow", high="brown", na.value = "white") + theme_classic() +
  theme(legend.position = "bottom")

p4 <- plot_ordination(phy_norm, ord, color = "EC") + geom_point(size=5) +
  scale_color_continuous(low="yellow", high="brown", na.value = "white") + theme_classic() +
  theme(legend.position = "bottom")

p5 <- plot_ordination(phy_norm, ord, color = "pH") + geom_point(size=5) +
  scale_color_continuous(low="yellow", high="brown", na.value = "white") + theme_classic() +
  theme(legend.position = "bottom")

layout_mat <- rbind(c(1,1,2),
                    c(3,4,5))

grid.arrange(p1, p2, p3, p4, p5, layout_matrix = layout_mat)
```

We can test whether the average soil relative humidity has a significant effect on the community composition with permutational multivariate analysis of variance. The function `adonis` from vegan package can be used for this. We need to remove NA's from the data before running `adonis`.
```{r adonis}
tmp <- prune_samples(!is.na(sample_data(phy_obj)$AverageSoilRelativeHumidity), phy_obj)
adonis(t(otu_table(tmp)) ~ AverageSoilRelativeHumidity, data = data.frame(sample_data(phy_obj)))
```

## Capscale

```{r, fig.width=8, fig.height=6}
tmp <- prune_samples(complete.cases(sample_data(phy_obj)), phy_obj)
plot(capscale(t(otu_table(tmp)) ~ PercentCover + AverageSoilTemperature + AverageSoilRelativeHumidity, 
              distance="bray", data=data.frame(sample_data(tmp))))
```

# Network analysis
For the network analysis we make a very simple example using phyloseq functions. To keep things simple, first remove taxa that have total abundance less than 50 counts.
```{r network, fig.width=10, fig.height=10}
phy_tmp <- prune_taxa(taxa_sums(phy_raref)>50, phy_raref)
ig <- make_network(phy_tmp, "taxa", distance = "jaccard", max.dist = 0.95)
plot_network(ig, phy_obj, type="taxa", point_size = 5, label=NULL, color="Class", line_alpha = 0.05)
ig <- make_network(phy_tmp, "samples", distance = "jaccard", max.dist = 0.95)
plot_network(ig, phy_obj, type="samples", point_size = 5, label=NULL, color="Vegetation", line_alpha = 0.05)
```

# Differentially abundant taxa
As a example we'll study how the vegetation affects the abundance of different ASVs in the samples. To keep it simple, we don't consider any other factors that in reality probably have an effect.  

We will use package called DESeq2, which uses a negative binomial distribution. DESeq2 is meant for RNA-seq data but works for also for microbiome count data such as ours. You can read more about DESeq2 and the statistics behind from [here](http://www.bioconductor.org/packages/release/bioc/vignettes/DESeq2/inst/doc/DESeq2.html) and [here.](http://web.stanford.edu/class/bios221/book/Chap-CountData.html)

Convcert the phyloseq object to DESeq2 object and define the factor we want to study (`Vegetation`).
```{r diff_abund}
diagdds = phyloseq_to_deseq2(phy_obj, ~ Vegetation)
diagdds$Vegetation <- relevel(diagdds$Vegetation, ref = "no")
diagdds = DESeq(diagdds, test="Wald", fitType="mean", sfType="poscount")
res = results(diagdds, cooksCutoff = FALSE, alpha = 0.01)

sigtab = cbind(as(res, "data.frame"), as(tax_table(phy_obj)[rownames(res), ], "matrix"))
sigtab[order(sigtab$log2FoldChange),] %>% head(10)
plot(t(otu_table(phy_obj)["45e4be8e16753ffd4bb4872bf6a35f37",]) ~ sample_data(phy_obj)$Vegetation,
     ylab="raw ASV count", xlab="Vegetation", main = "ASV: 45e4be8e16753ffd4bb4872bf6a35f37\nBacillus")
```

# Heatmap of differentially abundant taxa

```{r heatmap, fig.width=10, fig.height=6}
library(pheatmap)
diff_taxa <- row.names(sigtab[order(sigtab$log2FoldChange),]) %>% head(20)
phy_diff <-  prune_taxa(diff_taxa, phy_norm)
phy_diff <- prune_samples(sample_sums(phy_diff)>0, phy_diff)
pheatmap(sqrt(t(otu_table(phy_diff))), labels_col = tax_table(phy_diff)[,6],
         labels_row = sample_data(phy_diff)$Vegetation, cluster_cols = FALSE, angle_col = 45)
```

# Data wrangling with phyloseq
Some useful functions in phyloseq for wrangling your data before analyses. Some you have already used and here's a few more.
Before you use any of these to you're own data, read the manual and be sure what it actually does. More examples [here](https://joey711.github.io/phyloseq/) and full documentation with `?function_name`

## Accessing your data
In addition to the `otu_table`, `tax_table` and other functions to access your data, there's also some other very useful functions.
```{r access}
phy_obj
ntaxa(phy_obj)
nsamples(phy_obj)
sample_names(phy_obj) %>% head
rank_names(phy_obj)
sample_variables(phy_obj)
taxa_names(phy_obj) %>% head
```

## Merging samples
If you look at the map, there's fewer locations than we have samples. There is several samples from each location. Although they are nto actually replicates, we treat them as relicates now and will combine these together using the variable `SiteName`in the metadata.  

__Beware__ that the function sums up the counts, even if you specify `fun = "mean"`, so you need to divide the counts with the number of replicates you have.
```{r merge}
phy_mrg <- merge_samples(phy_obj, group = "SiteName", fun = "mean")
barplot(sample_sums(phy_mrg), las=3, main="Sum of counts", col = "skyblue")
otu_table(phy_mrg) <- otu_table(phy_mrg)[, ]/as.matrix(table(sample_data(phy_obj)$SiteName))[, 1]
barplot(sample_sums(phy_mrg), las=3, main="Mean counts", col = "skyblue")
```

## Agglomerate taxa on different taxonomic levels
This function lets you count taxa abundances on different taxonomic levels.
__Beware__, this function is very slow.

```{r glom}
phy_glom <- tax_glom(phy_obj, taxrank = "Phylum")
tax_table(phy_glom)
```

## Prune and subset samples and taxa
Different ways of getting rid off of samples or taxa from your data.

```{r prune}
prune_samples(sample_sums(phy_obj)>2000, phy_obj)
prune_taxa(taxa_sums(phy_obj)>100, phy_obj)
filter_taxa(phy_obj, function(x) sum(x>0) > (0.5*length(x)), TRUE)
subset_samples(phy_obj, TransectName=="Baquedano")
subset_taxa(phy_obj, Kingdom=="Bacteria")
```
